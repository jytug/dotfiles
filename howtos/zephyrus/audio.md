## What you need to use linux as an audio workstation

- pipewire -- install `manjaro-pipewire pipewire-jack pipewire-pulse`
- qpwgraph -- to control where audio comes and goes
- guitarix -- for playing guitar
- bitwig -- install from aur rather than flatpak
- yabridge -- for windows VSTs

## How to run guitarix
```
PIPEWIRE_LATENCY=256/44100 guitarix
```
You may need to play with qpwgraph to hear yourself
