#!/usr/bin/env sh

echo "Installing ZSH"

sudo pacman -S zsh

echo "Installing oh-my-zsh"
sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"

echo "Changing default shell to zsh"

